#Created by Phapawarin Phommas 
#Last Modified : 10/08/2020 
##########################################################
from selenium import webdriver
import pandas as pd
import os
from time import sleep


path = 'C:/Users/OS/AppData/Local/Programs/Python/Python38-32/'
os.chdir(path)

#Open driver
driver = webdriver.Chrome(path+"Chromedriver.exe")
driver.get('https://www.imdb.com/chart/top?sort=us,desc&mode=simple&page=1')
sleep(5.0)

#Get movieName
movienames = driver.find_elements_by_class_name('titleColumn')
Movienames = [name.text for name in movienames]

#Get rating
ratings = driver.find_elements_by_class_name('ratingColumn.imdbRating')
Ratings = [rating.text for rating in ratings]


#Get link list
links = driver.find_elements_by_xpath("//td[@class='titleColumn']/a")
Links = [link.get_attribute('href') for link in links]

Director = []
Actornames = []
Characternames = []   
Storyline = []
Genres = []
Releasedate = []
Runtime = []

#Loop to get information in each link
for l in range(len(Links)):
    driver.get(Links[l])     
    sleep(5.0)

    #Get Director
    director = driver.find_element_by_xpath("//div[@class='credit_summary_item']/a").text
    
    #Get casts (ActorNames,CharactorNames)
    casts = driver.find_elements_by_xpath("//table[@class='cast_list']/tbody/tr")
    actorNames =[cast.text.split('...')[0] for cast in casts if str(cast.text.find('...')).isnumeric()]
    characterNames =[cast.text.split('...')[1] for cast in casts if str(cast.text.find('...')).isnumeric()]

    #Get storyline
    storyine = driver.find_element_by_xpath("//div[@class='inline canwrap']/p/span").text
    genres = driver.find_elements_by_xpath("//*[@id='titleStoryLine']/div[4]")
    genres2 = [genres1.text for genres1 in genres]

    #Get Details  (releaseDate,runTime)
    detail = driver.find_element_by_id("titleDetails").text
    releaseDate=detail[detail.find("Release Date:")+14:]
    releaseDate=releaseDate[0:releaseDate.find("See")-1]
    runTime=detail[detail.find("Runtime:")+9:]
    runTime=runTime[0:runTime.find("min")-1] 

    #Append data to each list
    Director.append(director)
    Cast.append(casts)
    Actornames.append(actorNames)
    Characternames.append(characterNames)
    Storyline.append(storyine)
    Genres.append(genres2)
    Releasedate.append(releaseDate)
    Runtime.append(runTime)


#create pandas dataFrame 
DF_IMDb = pd.DataFrame({'MovieNames':Movienames, 'Ratings' : Ratings, 'Director' : Director, 'Actornames' : Actornames, 'Characternames':Characternames,\
                         'Storyline' : Storyline, 'Genres': Genres, 'Releasedate': Releasedate, 'Runtime':Runtime}) 

#export to excel                         
DF_IMDb.to_excel('output_data_imdb.xlsx')
